$(document).ready(function () {
	// init Isotope
	var $grid = $('.grid').isotope({
		itemSelector: '.element-item'
		, layoutMode: 'fitRows'
	});
	// filter functions
	var filterFns = {
		// show if number is greater than 50
		numberGreaterThan50: function () {
			var number = $(this).find('.number').text();
			return parseInt(number, 10) > 50;
		}
		, // show if name ends with -ium
		ium: function () {
			var name = $(this).find('.name').text();
			return name.match(/ium$/);
		}
	};
	// bind filter button click
	$('#filters').on('click', 'button', function () {
		var filterValue = $(this).attr('data-filter');
		// use filterFn if matches value
		filterValue = filterFns[filterValue] || filterValue;
		$grid.isotope({
			filter: filterValue
		});
	});
	// change is-checked class on buttons
	$('.button-group').each(function (i, buttonGroup) {
		var $buttonGroup = $(buttonGroup);
		$buttonGroup.on('click', 'button', function () {
			$buttonGroup.find('.is-checked').removeClass('is-checked');
			$(this).addClass('is-checked');
		});
	});
	var baseMargin = 20;
	$('.item').hover(function () {
		$(this).stop().animate({
			backgroundSize: '130%'
		}, 500);
		$('.innerslider', this).stop().animate({
			top: '-50px'
			, opacity: 1
		}, 500);
		$('.overslider', this).stop().animate({
			opacity: 0.6
		}, 500);
	}, function () {
		$(this).stop().animate({
			backgroundSize: '100%'
		}, 500);
		$('.innerslider', this).stop().animate({
			top: '100px'
			, opacity: 0
		}, 500);
		$('.overslider', this).stop().animate({
			opacity: 0
		}, 500);
	});
	$('div.magnificar img').hover(function () {
		$(this).animate({
			'width': 400
			, 'height': 400
			, 'margin': '-100px -90px'
		});
	}, function () {
		$(this).animate({
			'width': 200
			, 'height': 200
			, 'margin': '0 10px'
		});
	});
	$('.navbar-nav li').hover(function () {
		$('.navbar-nav-subitems', this).stop().animate({
			top: '60px'
			, opacity: 1
		}, 300);
	}, function () {
		$('.navbar-nav-subitems', this).stop().animate({
			top: '30px'
			, opacity: 0
		}, 300);
	});
	$(".navbar-brand img").css("width", "67%");
	$(window).bind('scroll', function () {
		$(".fadeleft").each(function () {
			if ($(window).scrollTop() >= $(this).offset().top - window.innerHeight) {
				//alert('end reached');
				$(this).css("animation-name", "fadeInLeft");
			}
		});
	});





	$(".navbar-nav li a").mouseover(function () {
		$(this).addClass("hover-change");
	});
	$(".navbar-nav li a").mouseout(function () {
		$(this).removeClass("hover-change");
	});
	$(".navbar-nav-subitems li").first().css("marginTop", "20px");
	$(".navbar-nav-subitems li").last().css("marginBottom", "20px");
});
var wheight = $(window).height();
$(window).scroll(function () {
	if ($(document).scrollTop() > wheight / 3) {
		$(".animation").addClass("color-change");
		$(".main-nav.animation").removeClass("color-change");
		$(".main-nav.animation").addClass("color-change2");
		$(".navbar-nav li a").css("color", "#323232");
		$(".navbar-nav-subitems li a").css("color", "#cccccc");
		$(".logo-light").css("opacity", 0);
		$(".logo-dark").css("opacity", 1);
		$(".navbar-nav li").css('padding-top', "10px");
		$(".navbar-nav li").css('padding-bottom', "10px");
		$(".navbar-nav li").css('padding-bottom', "10px");
		$(".navbar-brand img").css("width", "60%");
		$(".fa-search").css("color", "#323232");
		$(".fa-search").css("opacity", 1);
		$(".logo a img").css("margin-top", "18px");
		var basicoheight = $(".basico").height();
		$(".main-nav").height(basicoheight);
	}
	else {
		$(".animation").removeClass("color-change");
		$(".main-nav.animation").removeClass("color-change2");
		$(".navbar-nav li a").css("color", "#ffffff");
		$(".navbar-nav-subitems li a").css("color", "#cccccc");
		$(".logo-light").css("opacity", 1);
		$(".logo-dark").css("opacity", 0);
		$(".navbar-nav li").css('padding-top', "27px");
		$(".navbar-nav li").css('padding-bottom', "27px");
		$(".navbar-brand img").css("width", "67%");
		$(".fa-search").css("color", "#ffffff");
		$(".fa-search").css("opacity", 1);
		$(".logo a img").css("margin-top", "31px");
		var basicoheight = $(".basico").height();
		$(".main-nav").height(basicoheight);
	}
});
var vnext;
var next = 0;
var wwidth;
$(document).ready(function () {
	wwidth = $(window).width();
	$(".owl-item").width(wwidth);
	var otrosss = $(".owl-wrapper .owl-item").length+1;
	var itemnumber = 1;
	
		$(".owl-wrapper-outer").width(otrosss*wwidth);
		var repetir = $( ".owl-wrapper").find(".owl-item").html();
	$( ".owl-wrapper").find(".owl-item").first().addClass("torepeat");
	$(".owl-next").click(function () {
		//if (next < otrosss - 1) {
			next++;
		//}
		//else {
			//next = 0;
		//}
		vnext = next * wwidth;
		
		$(".owl-wrapper").css("transform", "translate3d(-" + vnext + "px,0px,0px)");		
		$( ".owl-wrapper" ).append('<div class="owl-item" style="width:'+wwidth+'px">'+repetir+'</div>' );		
		$( ".owl-wrapper").find(".torepeat").next().addClass("torepeat");		
		$( ".owl-wrapper").find(".torepeat").first().removeClass("torepeat");		
		repetir = $( ".owl-wrapper").find(".torepeat").first().html();
		
		/*	
		$(".owl-wrapper").css("transform", "translate3d(-" + wwidth + "px,0px,0px)");			
		$( ".torepeat" ).appendTo(".owl-wrapper ");
		$( ".owl-item").removeClass("torepeat");
		$( ".owl-wrapper").find(".owl-item").first().addClass("torepeat");
		
*/
		
		var anchocont = $(".owl-wrapper-outer").width();
		anchocont = anchocont + wwidth;
		$(".owl-wrapper-outer").width(anchocont);

	});

	

	
	function moveItem() {
		$(".owl-next").trigger('click');
	}
	setInterval(moveItem, 6000);
	$(".owl-prev").click(function () {
		if (next == 0) {
			next = otrosss - 1;
		}
		else {
			next--;
		}
		vnext = next * wwidth;
		$(".owl-wrapper").css("transform", "translate3d(-" + vnext + "px,0px,0px)");
	})
});
$(window).resize(function () {
	wwidth = $(window).width();
	$(".owl-item").width(wwidth);
	vnext = next * wwidth;
	$(".owl-wrapper").css("transform", "translate3d(-" + vnext + "px,0px,0px)");
	submenu();
	var hasimagewidth = $(".hasimage").width();
	//$(".hasimage").height(hasimagewidth*1.2);
	var docwidth = $(document).width();
	if (docwidth < 1200) {
		$('.div2:parent').each(function () {
			$(this).insertBefore($(this).prev('.div1'));
		});
	}
	else {
		$('.div1:parent').each(function () {
			$(this).insertBefore($(this).prev('.div2'));
		});
	}
});
$(document).ready(function () {

				$(".title-heading-left").css("animation-name", "fadeInUp");



	var ruta = $(location).attr('pathname');
	$(".navbar-nav li a").each(function () {
		var activo = $(this).attr('href');
		if (activo == ruta) {
			$(this).addClass("active");
		}
	});
	submenu();
	var hasimagewidth = $(".hasimage").width();
	//$(".hasimage").height(hasimagewidth*1.2);
	var docwidth = $(document).width();
	if (docwidth < 1200) {
		$('.div2:parent').each(function () {
			$(this).insertBefore($(this).prev('.div1'));
		});
	}
});

function submenu() {
	$(".navbar-nav li a").each(function () {
		var valor = $(this).text();
		if (valor == "Solutions") {
			var solpos = $(this).position();
			$(this).attr('href', "#");
			$(".navbar-nav-subitems").css("left", solpos.left);
		}
	});
}