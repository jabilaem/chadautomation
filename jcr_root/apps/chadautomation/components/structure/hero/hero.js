"use strict";
use(function () {
    var CONST = {
        PROP_TITLE: "jcr:title",
        PROP_SUBTITLE: "jcr:subtitle",
        PROP_BUTTON1_TEXT: "jcr:button1",
        PROP_BUTTON2_TEXT: "jcr:button2",
        PROP_BUTTON1_LINK: "jcr:button_1_linkURL",
        PROP_BUTTON2_LINK: "jcr:button_2_linkURL",
        PROP_REF_HERO_IMAGE: "fileReference",
        PROP_UPLOAD_HERO_IMAGE: "file",
        PROP_BACKOVER: "backover",
        //PROP_DEFAULT_BACKOVER: "backover-white",
        PROP_UPPERCASE: "uppercase"
    };
    
    var hero = {};
    
    //Get the title text
    hero.text = properties.get(CONST.PROP_TITLE)
            || pageProperties.get(CONST.PROP_TITLE);
    
    //Get the title subtext
    hero.subtext = properties.get(CONST.PROP_SUBTITLE)
            || pageProperties.get(CONST.PROP_SUBTITLE);


    
    //Get the title text
    hero.button1 = properties.get(CONST.PROP_BUTTON1_TEXT)
            || pageProperties.get(CONST.PROP_BUTTON1_TEXT);

    hero.button2 = properties.get(CONST.PROP_BUTTON2_TEXT)
            || pageProperties.get(CONST.PROP_BUTTON2_TEXT);

    hero.button1_link = properties.get(CONST.PROP_BUTTON1_LINK)
            || pageProperties.get(CONST.PROP_BUTTON1_LINK);

    hero.button2_link = properties.get(CONST.PROP_BUTTON2_LINK)
            || pageProperties.get(CONST.PROP_BUTTON2_LINK);
    
    if (hero.button1  &&  hero.button1_link) {        
        hero.style1 = "display: inline-block!important;";
    }else{
			
        hero.style1 = "display: none!important;";
		}

    if (hero.button2 &&  hero.button2_link ) {        
        hero.style2 = "display: inline-block!important;";
    }else{
			
        hero.style2 = "display: none!important;";
		}

    
    //Check for file reference from the DAM
    var image = properties.get(CONST.PROP_REF_HERO_IMAGE, String.class);
    if (image == "undefined") {
        //Check for file upload
            var res = resource.getChild(CONST.PROP_UPLOAD_HERO_IMAGE);
        if (res != null) {
            image = res.getPath();
        }
    }
    
    if(image != "undefined") {
        hero.style = "background-image:url(" + image + ");";
    }
    if (currentStyle.get(CONST.PROP_UPPERCASE)) {
        hero.uppercase = "text-transform: uppercase;";
    }

        hero.backover = granite.resource.properties[CONST.PROP_BACKOVER]
            || currentStyle.get(CONST.PROP_DEFAULT_BACKOVER, "");



    if (hero.backover == "backover-white") {
        //$(".animation").addClass("color-change2");
    }  

    return hero;
});