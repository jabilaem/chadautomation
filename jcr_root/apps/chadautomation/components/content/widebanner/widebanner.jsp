<jsp:directive.include file="/libs/foundation/global.jsp"/>
<!--<cq:includeClientLib categories="bootstrap" />-->
<jsp:directive.page session="false" />


<style>
		.demo-border-blue{
    border:1px solid #0066FF;
} 
        .demo-border-black{
    border:1px solid #000000;
} 
        .demo-border-green{
    border:1px solid #19D1A3;
} 
        .demo-border-red{
    border:1px solid #FF4D4D;
} 
        .demo-border-none{
    border:0;
} 
        .bgcolor-none{
     background-color: transparent;
}  
        .bgcolor-white{
     background-color: #ffffff;
}  
	.bgcolor-blue{
    background-color: #0066FF;
} 
        .bgcolor-black{
     background-color: #000000;
} 
        .bgcolor-green{
     background-color:  #19D1A3;
} 
        .bgcolor-red{
     background-color:  #FF4D4D;
} 
        .bgcolor-lgrey{
     background-color:  #f7f7f7;
} 
        .fontcolor-white{
     color:  #FFffff!important;
} 
        .fontcolor-black{
     color:  #000000!important;
} 
        .fontcolor-inherit{
     color:  #888!important;
}  
        .bgexpand{
     background-size: cover;
}  
        .bgfloat{
     background-size: cover;
     background-attachment: fixed!important;
} 
.mt-10{
     margin-top: 10px;
}  
.mt-20{
     margin-top:20px;
}  
.mt-15{
     margin-top:15px;
}  
.mt-30{
     margin-top:30px;
} 
.mt-60{
     margin-top:60px;
} 
.mt-none{
     margin-top:0;
} 
.mt-default{
     margin-top:inherit;
} 

.mb-10{
     margin-bottom: 10px;
}  
.mb-20{
     margin-bottom:20px;
}  
.mb-15{
     margin-bottom:15px;
}  
.mb-30{
     margin-bottom:30px;
} 
.mb-none{
     margin-bottom:0;
}  
.mb-60{
     margin-bottom:60px;
} 
.mb-default{
     margin-bottom:inherit;
}  

.pt-10{
     padding-top: 10px;
}  
.pt-20{
     padding-top:20px;
}  
.pt-15{
     padding-top:15px;
}  
.pt-30{
     padding-top:30px;
} 
.pt-60{
     padding-top:60px;
} 
.pt-none{
     padding-top:0;
} 
.pt-default{
     padding-top:inherit;
} 

.pb-10{
     padding-bottom: 10px;
}  
.pb-20{
     padding-bottom:20px;
}  
.pb-15{
     padding-bottom:15px;
}  
.pb-30{
     padding-bottom:30px;
} 
.pb-none{
     padding-bottom:0;
}  
.pb-60{
     padding-bottom:60px;
} 
.pb-default{
     padding-bottom:inherit;
}  

.ml-10{
     padding-left: 10px;
}  
.ml-20{
     padding-left:20px;
}  
.ml-15{
     padding-left:15px;
}  
.ml-30{
     padding-left:30px;
} 
.ml-60{
     padding-left:60px;
} 
.ml-none{
     padding-left:0;
}  
.ml-default{
     padding-left:inherit;
}  

.mr-10{
     padding-right: 10px;
}  
.mr-20{
     padding-right:20px;
}  
.mr-15{
     padding-right:15px;
}  
.mr-30{
     padding-right:30px;
} 
.mr-60{
     padding-right:60px;
} 
.mr-none{
     padding-right:0;
} 
.mr-default{
     padding-right:inherit;
} 
.centrar{
     display: flex;
    align-items: center;
}  


    </style>
<c:set var = "color" value = "${properties.color}" />
<c:set var = "bgcolor" value = "${properties.bgcolor}" />
<c:set var = "fontcolor" value = "${properties.fontcolor}" />
<c:set var = "check" value = "${properties.expand}" />
<c:set var = "bgimage" value = "${properties.bgimage}" />
<c:set var = "bgexpand" value = "${properties.bgexpand}" />
<c:set var = "bgfloat" value = "${properties.bgfloat}" />
<c:set var = "mgtop" value = "${properties.mgtop}" />
<c:set var = "pttop" value = "${properties.pttop}" />
<c:set var = "mgbottom" value = "${properties.mgbottom}" />
<c:set var = "pbbottom" value = "${properties.pbbottom}" />
<c:set var = "mgleft" value = "${properties.mgleft}" />
<c:set var = "mgright" value = "${properties.mgright}" />
<c:set var = "minheight" value = "${properties.minheight}" />
<c:set var = "vcenter" value = "${properties.vcenter}" />
<c:set var = "backover" value = "${properties.overlay}" />
<c:set var = "animation" value = "${properties.animation}" />

<div class="row">
    <div class="col-sm-12 ${mgtop} ${mgbottom} ${pttop} ${pbbottom} ${mgleft}  ${mgright} ${color} ${fontcolor} ${bgcolor} ${bgexpand} ${bgfloat} ${vcenter} ${backover} ${animation} animated" style="background-image:url('${bgimage}'); 
    background-position: center;background-repeat:no-repeat;min-height:${minheight}px">
                    <div style="margin:auto; width:100%"><cq:include path="col-12-1" resourceType="/libs/foundation/components/parsys" /></div>
                    </div>
</div>
