"use strict";
use(function () {
    var CONST = {
        PROP_BUTTON1_TEXT: "jcr:button1",
        PROP_BUTTON1_LINK: "jcr:button_1_linkURL",
        PROP_ALIGN: "alignment",
        PROP_EXTERNAL: "external",
        PROP_MTOP: "mtop",
        PROP_MBOT: "mbot",
        PROP_NTAB: "ntab",
        PROP_FULLWIDTH: "fullwidth"
    };
    
    var cta = {};
    cta.button1 = properties.get(CONST.PROP_BUTTON1_TEXT)
            || pageProperties.get(CONST.PROP_BUTTON1_TEXT);

    if(cta.button1 == ''){
cta.button1 = "Add text"
    }

    cta.button1_link = properties.get(CONST.PROP_BUTTON1_LINK)
            || pageProperties.get(CONST.PROP_BUTTON1_LINK);
    

    if (properties.get(CONST.PROP_FULLWIDTH)) {        
        cta.fullwidth = "width: 100%;";
    }
    if (!properties.get(CONST.PROP_EXTERNAL)) {        
        cta.internal = ".html";
    }

    if (properties.get(CONST.PROP_NTAB)) {        
        cta.ntab = "_blank";
    }

        cta.alignment = granite.resource.properties[CONST.PROP_ALIGN]
            || currentStyle.get(CONST.PROP_ALIGN, "");



    if (cta.alignment == "left") {
        cta.alignment = "text-align: left;";
    } 
       if (cta.alignment == "right") {
        cta.alignment = "text-align: right;";
    } 
       if (cta.alignment == "center") {
        cta.alignment = "text-align: center;";
    } 

    cta.mtop = granite.resource.properties[CONST.PROP_MTOP]
            || currentStyle.get(CONST.PROP_MTOP, "");



    if (cta.mtop == "m10") {
        cta.mtop = "margin-top: 10px;";
    } 
       if (cta.mtop == "m20") {
        cta.mtop = "margin-top: 20px;";
    } 
       if (cta.mtop == "m30") {
        cta.mtop = "margin-top: 30px;";
    } 
       if (cta.mtop == "m40") {
        cta.mtop = "margin-top: 40px;";
    } 
       if (cta.mtop == "m50") {
        cta.mtop = "margin-top: 50px;";
    } 


    cta.mbot = granite.resource.properties[CONST.PROP_MBOT]
            || currentStyle.get(CONST.PROP_MBOT, "");



    if (cta.mbot == "m10") {
        cta.mbot = "margin-bottom: 10px;";
    } 
       if (cta.mbot == "m20") {
        cta.mbot = "margin-bottom: 20px;";
    } 
       if (cta.mbot == "m30") {
        cta.mbot = "margin-bottom: 30px;";
    } 
       if (cta.mbot == "m40") {
        cta.mbot = "margin-bottom: 40px;";
    } 
       if (cta.mbot == "m50") {
        cta.mbot = "margin-bottom: 50px;";
    } 




    return cta;
});