<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@page session="false" %>

<div style="display: block;
     position: relative!important;
    z-index: 0!important;">


<%
        try {
            Property property = null;
            Property responsive = null;

            if (currentNode.hasProperty("countries")) {
                property = currentNode.getProperty("countries");
            }

            if (currentNode.hasProperty("behavior")) {
                responsive = currentNode.getProperty("behavior");
            }

            if (property != null) {
                JSONObject country = null, slideTitle = null, displayStyle = null, type = null, slideimageURL = null,slideDesc = null, typeTxt = null;
                Value[] values = null;

                if (property.isMultiple()) {
                    values = property.getValues();
                } else {
                    values = new Value[1];
                    values[0] = property.getValue();
                }

	%> 

        <section id="contant" class="wow fadeIn ptb ptb-sm-80 animated" style="visibility: visible; animation-name: fadeIn;">
  <div class="owl-carousel content-carousel content-slider owl-theme" style="opacity: 1; display: block;">
    <div class="owl-wrapper-outer autoHeight" style="width: 13490px;">
        <div class="owl-wrapper" style="left: 0px; display: block; transition: all 800ms ease; transform: translate3d(0px, 0px, 0px);">

	<%     int vuelta = 1;
                for (Value val : values) {
                    country = new JSONObject(val.getString());
                    slideimageURL = new JSONObject(val.getString());



	%>
<% 

    if (vuelta == 1){
%>
            <div class="owl-item"> 
          <div class="item">
            <div class="container">

<%
}
	 %>


                <div class="col-md-4 mb-sm-30">
                  <img src="<%= country.get("slideimageURL") %>"  class="img-responsive">
                </div>





<% 

vuelta++;
  if (vuelta == 4){

%>
              </div>
            </div>
        </div>


<%


vuelta = 1;

}}
	 %>
	  </div>
    </div>       
    <div class="owl-controls clickable">
      <div class="owl-buttons">
        <div class="owl-prev">
        </div>
        <div class="owl-next">
        </div>
      </div>
    </div>
  </div>
</section>
	<%
            } else {
%>
                Add values in dialog
                <br><br>
<%
            }
        } catch (Exception e) {
            e.printStackTrace(new PrintWriter(out));
        }
%>	
</div>
