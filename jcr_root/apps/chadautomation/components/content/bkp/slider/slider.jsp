<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@page session="false" %>

    <div class="infiniteCarousel">
      <div class="wrapper">
        <ul>
<%
        try {
            Property property = null;

            if (currentNode.hasProperty("countries")) {
                property = currentNode.getProperty("countries");
            }

            if (property != null) {
                JSONObject country = null, slideTitle = null, displayStyle = null, type = null, slideimageURL = null, slideDesc = null, typeTxt = null;
                Value[] values = null;

                if (property.isMultiple()) {
                    values = property.getValues();
                } else {
                    values = new Value[1];
                    values[0] = property.getValue();
                }
	 
	 


	%> 



	 
	 
	 
	<% 
	 
                for (Value val : values) {
                    country = new JSONObject(val.getString());
                    slideDesc = new JSONObject(val.getString());
		 
	%>


		
		
          <li><img src="<%= country.get("slideDesc") %>"/></li>
<%
                }
	 %>


	<%
            } else {
%>
                Add values in dialog
                <br><br>
<%
            }
        } catch (Exception e) {
            e.printStackTrace(new PrintWriter(out));
        }
%>

        </ul>        
      </div>
    </div>
