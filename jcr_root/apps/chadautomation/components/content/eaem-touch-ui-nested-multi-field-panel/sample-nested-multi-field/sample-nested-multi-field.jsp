<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@page session="false" %>

<div style="display: block;
     position: relative!important;
    z-index: 0!important;">
<ul class="fusion-checklist fusion-checklist-1" style="font-size:14px;line-height:23.8px;">

<%
        try {
            Property property = null;

            if (currentNode.hasProperty("countries")) {
                property = currentNode.getProperty("countries");
            }

            if (property != null) {
                JSONObject country = null, slideTitle = null, displayStyle = null, type = null, slideimageURL = null, slideDesc = null, typeTxt = null;
                Value[] values = null;

                if (property.isMultiple()) {
                    values = property.getValues();
                } else {
                    values = new Value[1];
                    values[0] = property.getValue();
                }

	 


	%> 



	 
	 
	 
	<% 
	 
                for (Value val : values) {
                    country = new JSONObject(val.getString());
                    slideDesc = new JSONObject(val.getString());
		 
	%>


    <li class="fusion-li-item">
        <span style="background-color:#702f8a;font-size:12.32px;height:23.8px;width:23.8px;margin-right:9.8px;" class="icon-wrapper circle-yes"><i class="fusion-li-icon fa-check fas" style="color:#ffffff;"></i></span>
        <div class="fusion-li-item-content" style="margin-left:33.6px;">
        <p><%= country.get("slideDesc") %></p>
        </div>
    </li>

<%
                }
	 %>


	<%
            } else {
%>
                Add values in dialog
                <br><br>
<%
            }
        } catch (Exception e) {
            e.printStackTrace(new PrintWriter(out));
        }
%>
	<!--
  <button class="button tabs" data-filter=".automotive">AUTOMOTIVE</button>	
  <button class="button tabs" data-filter=".security">SECURITY</button>
  <button class="button tabs" data-filter=".consumer">CONSUMER ELECTRONICS</button>

-->
	</ul>
</div>
