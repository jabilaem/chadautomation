<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@page session="false" %>

<div class="fusion-content-boxes content-boxes columns row fusion-columns-1 fusion-columns-total-9 fusion-content-boxes-2 content-boxes-timeline-vertical content-left content-boxes-icon-with-title fusion-delayed-animation" data-animation-delay="350" data-animationoffset="100%" style="margin-top:0px;margin-bottom:60px;">
	<style type="text/css" scoped="scoped">.fusion-content-boxes-2 .heading h2{color:#333333;}
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading h2,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .heading-link h2,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading h2,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .heading-link h2,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::after,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover.link-area-box .fusion-read-more::before,
	.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:after,
	.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover:before,
	.fusion-content-boxes-2 .fusion-content-box-hover .fusion-read-more:hover,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::after,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover.link-area-box .fusion-read-more::before,
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .icon .circle-no,
	.fusion-content-boxes-2 .heading .heading-link:hover .content-box-heading {
		color: #702f8a;
	}
	.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .icon .circle-no {
		color: #702f8a !important;
		}.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button {background: #00b7ef;color: #ffffff;}.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box.link-area-box-hover .fusion-content-box-button .fusion-button-text {color: #ffffff;}
		.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon > span {
			background-color: #702f8a !important;
		}
		.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .icon > span {
			border-color: #702f8a !important;
		}</style>


		<div class="fusion-column content-box-column content-box-column content-box-column-1 col-lg-12 col-md-12 col-sm-12 fusion-content-box-hover  content-box-column-last-in-row fusion-appear">
<%
        try {
            Property property = null;

            if (currentNode.hasProperty("countries")) {
                property = currentNode.getProperty("countries");
            }

            if (property != null) {
                JSONObject country = null, slideTitle = null, slideDesc = null, last = null;
                Value[] values = null;

                if (property.isMultiple()) {
                    values = property.getValues();
                } else {
                    values = new Value[1];
                    values[0] = property.getValue();
                }
	 
	 


	%> 



	 
	 
	 
	<% 
	 
                for (Value val : values) {
                    country = new JSONObject(val.getString());
                    slideTitle = new JSONObject(val.getString());
                    slideDesc = new JSONObject(val.getString());
                    last = new JSONObject(val.getString());
		 
	%>



            <div class="col content-wrapper link-area-link-icon content-icon-wrapper-yes icon-hover-animation-fade fusion-animated " style="background-color: rgba(247, 247, 247, 0); visibility: visible; animation-duration: 0.25s;position:relative" data-animationtype="fadeIn" data-animationduration="0.25" data-animationoffset="100%">
				<div class="heading heading-with-icon icon-left">
					<div class="icon" style="-webkit-animation-duration: 350ms;animation-duration: 350ms;">
						<span style="height:16px;width:16px;line-height:8px;border-color:#e5e5e5;border-width:1px;border-style:solid;background-color:#333333;margin-right:20px;box-sizing:content-box;border-radius:50%;">
							<i style="border-color:#333333;border-width:0px;background-color:#e5e5e5;box-sizing:content-box;height:16px;width:16px;line-height:16px;border-radius:50%;position:relative;top:auto;left:auto;margin:0;border-radius:50%;color:#e5e5e5;font-size:8px;
    font-weight: 300;" class="fontawesome-icon fa-bullseye fas circle-yes icons">
							</i>
						</span>
					</div>
					<h2 class="content-box-heading" style="font-size:28px;line-height:33px;"><%= country.get("slideTitle") %></h2>
				</div>
				<div class="fusion-clearfix">
				</div>
				<div class="content-container" style="padding-left:38px;color:#747474;"><%= country.get("slideDesc") %></div>

                <div class="content-box-shortcode-timeline-vertical <%= country.get("last") %>" style="border-color:#e5e5e5;left:9px;top:14px;-webkit-transition-duration: 350ms;animation-duration: 350ms;
    position: absolute;">
				</div>
			</div>




<%
                }
	 %>


	<%
            } else {
%>
                Add values in dialog
                <br><br>
<%
            }
        } catch (Exception e) {
            e.printStackTrace(new PrintWriter(out));
        }
%>
		</div>


		
		<style type="text/css" scoped="scoped">
		.fusion-content-boxes-2 .fusion-content-box-hover .heading-link:hover .icon i.circle-yes,
		.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box:hover .heading-link .icon i.circle-yes,
		.fusion-content-boxes-2 .fusion-content-box-hover .link-area-link-icon-hover .heading .icon i.circle-yes,
		.fusion-content-boxes-2 .fusion-content-box-hover .link-area-box-hover .heading .icon i.circle-yes {
			background-color: #702f8a !important;
			border-color: #702f8a !important;
		}</style>
		<div class="fusion-clearfix">
		</div>
	</div>