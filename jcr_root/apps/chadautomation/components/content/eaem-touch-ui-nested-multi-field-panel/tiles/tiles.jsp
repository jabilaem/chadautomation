<%@ page import="org.apache.sling.commons.json.JSONObject" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="org.apache.sling.commons.json.JSONArray" %>
<%@ page import="java.util.*" %>
<%@include file="/libs/foundation/global.jsp" %>
<%@page session="false" %>
<style>
    .tileitem{
display: list-item;
    text-align: -webkit-match-parent;
    list-style: none;
    float: left;
    margin-right: 15px;
    max-width: 18%;
    }
        .laste{
                margin-right:0!important;
    }
    .imgitem{
        width: 90%;
    margin: auto;
        }
</style>
<ul>

    <%
        try {
            Property property = null;

            if (currentNode.hasProperty("countries")) {
                property = currentNode.getProperty("countries");
            }

            if (property != null) {
                JSONObject country = null, slideTitle = null, slideImage = null, linkPath = null, laste = null;
                Value[] values = null;

                if (property.isMultiple()) {
                    values = property.getValues();
                } else {
                    values = new Value[1];
                    values[0] = property.getValue();
                }
	 
	 


	%> 



	 
	 
	 
	<% 
	 
                for (Value val : values) {
                    country = new JSONObject(val.getString());
                    slideTitle = new JSONObject(val.getString());
                    slideImage = new JSONObject(val.getString());
                    linkPath = new JSONObject(val.getString());
                    laste = new JSONObject(val.getString());
		 
	%>




    	<li class="tileitem  <%= country.get("laste") %>">
		<div>
				<h4 style="text-align: center;"><a href="<%= country.get("linkPath") %>" target="_blank"><%= country.get("slideTitle") %></a></h4>
			</div>
			<div class="imgitem">
				<a href="<%= country.get("linkPath") %>" >
					<img src="<%= country.get("slideImage") %>" class="img-responsive">
				</a>
			</div>
	</li>




<%
                }
	 %>


	<%
            } else {
%>
                Add values in dialog
                <br><br>
<%
            }
        } catch (Exception e) {
            e.printStackTrace(new PrintWriter(out));
        }
%>
</ul>
<div style="clear:both"></div>