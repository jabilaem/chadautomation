<jsp:directive.include file="/libs/foundation/global.jsp"/>
<!--<cq:includeClientLib categories="bootstrap" />-->
<jsp:directive.page session="false" />

<jsp:scriptlet>
	pageContext.setAttribute("coltype", xssAPI.filterHTML(properties.get("coltype","")));


            Property responsive = null;

            if (currentNode.hasProperty("behavior")) {
                responsive = currentNode.getProperty("behavior");
            }
</jsp:scriptlet>

<c:if test="${empty coltype}">
	This is column control component
</c:if>

<c:if test="${not empty coltype}">
<style>
		.demo-border-blue{
    border:1px solid #0066FF;
} 
        .demo-border-black{
    border:1px solid #000000;
} 
        .demo-border-green{
    border:1px solid #19D1A3;
} 
        .demo-border-red{
    border:1px solid #FF4D4D;
} 
        .demo-border-none{
    border:0;
} 

.mt-10{
     margin-top: 10px;
}  
.mt-20{
     margin-top:20px;
}  
.mt-15{
     margin-top:15px;
}  
.mt-30{
     margin-top:30px;
} 
.mt-60{
     margin-top:60px;
} 
.mt-none{
     margin-top:0;
} 
.mt-default{
     margin-top:inherit;
} 

.mb-10{
     margin-bottom: 10px;
}  
.mb-20{
     margin-bottom:20px;
}  
.mb-15{
     margin-bottom:15px;
}  
.mb-30{
     margin-bottom:30px;
} 
.mb-none{
     margin-bottom:0;
}  
.mb-60{
     margin-bottom:60px;
} 
.mb-default{
     margin-bottom:inherit;
}  

.pt-10{
     padding-top: 10px;
}  
.pt-20{
     padding-top:20px;
}  
.pt-15{
     padding-top:15px;
}  
.pt-30{
     padding-top:30px;
} 
.pt-60{
     padding-top:60px;
} 
.pt-none{
     padding-top:0;
} 
.pt-default{
     padding-top:inherit;
} 

.pb-10{
     padding-bottom: 10px;
}  
.pb-20{
     padding-bottom:20px;
}  
.pb-15{
     padding-bottom:15px;
}  
.pb-30{
     padding-bottom:30px;
} 
.pb-none{
     padding-bottom:0;
}  
.pb-60{
     padding-bottom:60px;
} 
.pb-default{
     padding-bottom:inherit;
}  

.ml-10{
     padding-left: 10px;
}  
.ml-20{
     padding-left:20px;
}  
.ml-15{
     padding-left:15px;
}  
.ml-30{
     padding-left:30px;
} 
.ml-60{
     padding-left:60px;
} 
.ml-none{
     padding-left:0;
}  
.ml-default{
     padding-left:inherit;
}  

.mr-10{
     padding-right: 10px;
}  
.mr-20{
     padding-right:20px;
}  
.mr-15{
     padding-right:15px;
}  
.mr-30{
     padding-right:30px;
} 
.mr-60{
     padding-right:60px;
} 
.mr-none{
     padding-right:0;
} 
.mr-default{
     padding-right:inherit;
} 

    </style>
<c:set var = "column" value = "${properties.coltype}" />
<c:set var = "mgtop" value = "${properties.mgtop}" />
<c:set var = "pttop" value = "${properties.pttop}" />
<c:set var = "mgbottom" value = "${properties.mgbottom}" />
<c:set var = "pbbottom" value = "${properties.pbbottom}" />
<c:set var = "mgleft" value = "${properties.mgleft}" />
<c:set var = "mgright" value = "${properties.mgright}" />
<div class="row ${mgtop} ${mgbottom} ${pttop} ${pbbottom} ${mgleft}  ${mgright} ">
	<c:choose>

		<c:when test="${coltype eq '4-8-col'}">
            <% 
            if (responsive == null) {
			%>
	  		<div class="col-xs-12 col-sm-4 ${color}"><cq:include path="col-4-8-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-8 ${color}"><cq:include path="col-4-8-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
		  	<div class="div1 col-xs-12 col-sm-4 ${color}"><cq:include path="col-4-8-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-8 ${color}"><cq:include path="col-4-8-2" resourceType="/libs/foundation/components/parsys" /></div>
            <%
            } 
            %>
		</c:when>

        <c:when test="${coltype eq '6-6-col'}">
            <% 
            if (responsive == null) {
			%>
		  	<div class="col-xs-12 col-md-6 ${color}"><cq:include path="col-6-6-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-md-6 ${color}"><cq:include path="col-6-6-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
		  	<div class="div1 col-xs-12 col-md-6 ${color}"><cq:include path="col-6-6-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-md-6 ${color}"><cq:include path="col-6-6-2" resourceType="/libs/foundation/components/parsys" /></div>
            <%
            } 
            %>
		</c:when>


		<c:when test="${coltype eq '8-4-col'}">
            <% 
            if (responsive == null) {
			%>
		  	<div class="col-xs-12 col-sm-8" ><cq:include path="col-8-4-1" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-4"><cq:include path="col-8-4-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
		  	<div class="div1 col-xs-12 col-sm-8" ><cq:include path="col-8-4-1" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="div2 col-xs-12 col-sm-4"><cq:include path="col-8-4-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>

		<c:when test="${coltype eq '5-7-col'}">
            <% 
            if (responsive == null) {
			%>
		  	<div class="col-xs-12 col-sm-5 ${color}"><cq:include path="col-5-7-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-7 ${color}"><cq:include path="col-5-7-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
		  	<div class="div1 col-xs-12 col-sm-5 ${color}"><cq:include path="col-5-7-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-7 ${color}"><cq:include path="col-5-7-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>

        <c:when test="${coltype eq '7-5-col'}">
            <% 
            if (responsive == null) {
			%>
		  	<div class="col-xs-12 col-sm-7 ${color}"><cq:include path="col-7-5-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-5 ${color}"><cq:include path="col-7-5-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
		  	<div class="div1 col-xs-12 col-sm-7 ${color}"><cq:include path="col-7-5-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-5 ${color}"><cq:include path="col-7-5-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>



		<c:when test="${coltype eq '3-9-col'}">
            <% 
            if (responsive == null) {
			%>
			<div class="col-xs-12 col-sm-3  ${color}"><cq:include path="col-3-9-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-9  ${color}"><cq:include path="col-3-9-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
			<div class="div1 col-xs-12 col-sm-3  ${color}"><cq:include path="col-3-9-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-9  ${color}"><cq:include path="col-3-9-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>

		<c:when test="${coltype eq '9-3-col'}">
            <% 
            if (responsive == null) {
			%>
			<div class="col-xs-12 col-sm-9  ${color}"><cq:include path="col-9-3-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-3  ${color}"><cq:include path="col-9-3-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
			<div class="div1 col-xs-12 col-sm-9  ${color}"><cq:include path="col-9-3-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-3  ${color}"><cq:include path="col-9-3-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>

		<c:when test="${coltype eq '10-2-col'}">
            <% 
            if (responsive == null) {
			%>
			<div class="col-xs-12 col-sm-10  ${color}"><cq:include path="col-10-2-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-2  ${color}"><cq:include path="col-10-2-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
			<div class="div1 col-xs-12 col-sm-10  ${color}"><cq:include path="col-10-2-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-2  ${color}"><cq:include path="col-10-2-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>

		<c:when test="${coltype eq '2-10-col'}">
            <% 
            if (responsive == null) {
			%>
			<div class="col-xs-12 col-sm-2  ${color}"><cq:include path="col-2-10-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-10  ${color}"><cq:include path="col-2-10-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } else {
            %>
			<div class="div1 col-xs-12 col-sm-2  ${color}"><cq:include path="col-2-10-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="div2 col-xs-12 col-sm-10  ${color}"><cq:include path="col-2-10-2" resourceType="/libs/foundation/components/parsys" /></div>
			<%
            } 
            %>
		</c:when>



		<c:when test="${coltype eq '4-4-4-col'}">
		  	<div class="col-xs-12 col-sm-4 ${color}"><cq:include path="col-4-4-4-1" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-4 ${color}"><cq:include path="col-4-4-4-2" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-4 ${color}"><cq:include path="col-4-4-4-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '3-3-3-3-col'}">
		  	<div class="col-xs-12 col-sm-6 col-md-3 ${color}"><cq:include path="col-3-3-3-3-1" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-6 col-md-3 ${color}"><cq:include path="col-3-3-3-3-2" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-6 col-md-3 ${color}"><cq:include path="col-3-3-3-3-3" resourceType="/libs/foundation/components/parsys" /></div>
		 	<div class="col-xs-12 col-sm-6 col-md-3 ${color}"><cq:include path="col-3-3-3-3-4" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '2-2-2-2-2-2-col'}">
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-1" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-2" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-3" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-4" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-5" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-4 col-md-2 ${color}"><cq:include path="col-2-2-2-2-2-2-6" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
        <c:when test="${coltype eq '3-3-6-col'}">
			<div class="col-xs-12  col-sm-3 ${color}"><cq:include path="col-3-3-6-1" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12  col-sm-3 ${color}"><cq:include path="col-3-3-6-2" resourceType="/libs/foundation/components/parsys" /></div>
		  	<div class="col-xs-12 col-sm-6 ${color}"><cq:include path="col-3-3-6-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '6-3-3-col'}">
			<div class="col-xs-12 col-sm-6  ${color}"><cq:include path="col-6-3-3-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-3  ${color}"><cq:include path="col-6-3-3-2" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-3  ${color}"><cq:include path="col-6-3-3-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '3-6-3-col'}">
			<div class="col-xs-12 col-sm-3 ${color}"><cq:include path="col-3-6-3-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-6 ${color}"><cq:include path="col-3-6-3-2" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-3 ${color}"><cq:include path="col-3-6-3-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '2-8-2-col'}">
			<div class="col-xs-12 col-sm-2 ${color}"><cq:include path="col-2-8-2-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-8 ${color}"><cq:include path="col-2-8-2-2" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-2 ${color}"><cq:include path="col-2-8-2-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
		<c:when test="${coltype eq '1-10-1-col'}">
			<div class="col-xs-12 col-sm-1 ${color}"><cq:include path="col-1-10-1-1" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-10  ${color}"><cq:include path="col-1-10-1-2" resourceType="/libs/foundation/components/parsys" /></div>
	  		<div class="col-xs-12 col-sm-1 ${color}"><cq:include path="col-1-10-1-3" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>

		<c:when test="${coltype eq '1-2-2-2-2-2-1-col'}">
			<div class="hidden-xs col-sm-1 ${color}"><cq:include path="col-1-2-2-2-2-2-1-1" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-2 ${color}"><cq:include path="col-1-2-2-2-2-2-1-2" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-2 ${color}"><cq:include path="col-1-2-2-2-2-2-1-3" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-2 ${color}"><cq:include path="col-1-2-2-2-2-2-1-4" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-6 col-sm-2 ${color}"><cq:include path="col-1-2-2-2-2-2-1-5" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="col-xs-12 col-sm-2 ${color}"><cq:include path="col-1-2-2-2-2-2-1-6" resourceType="/libs/foundation/components/parsys" /></div>
			<div class="hidden-xs col-sm-1 ${color}"><cq:include path="col-1-2-2-2-2-2-1-7" resourceType="/libs/foundation/components/parsys" /></div>
		</c:when>
	</c:choose>
</div>
</c:if>
