"use strict";

use(function () {

    var CONST = {
        PROP_TITLE: "jcr:title",
        PROP_TAG_TYPE: "type",
        PROP_UPPERCASE: "uppercase",
        //PROP_TYPE: "type",
        PROP_DEFAULT_TYPE: "defaultType",
        PROP_FONTCOLOR: "fontcolor",
        PROP_DEFAULT_FONTCOLOR: "",
        PROP_ALIGN: "alignment",
        PROP_MARGINB: "marginb",
        PROP_BOLD: "bold",
        PROP_UNDERLINE: "underline"
    }
    
    var title = {};
    
    // The actual title content retrieved from the property title
	// or, the pageProperties title, or, the currentPage.name
    title.text = properties.get(CONST.PROP_TITLE)
            || pageProperties.get(CONST.PROP_TITLE)
            || currentPage.name;


    if (properties.get(CONST.PROP_UNDERLINE)) {
        title.underline = "display:block!important";
    } else {			
        title.underline = "display:none!important";
		}


    if (properties.get(CONST.PROP_MARGINB)) {
        title.marginb = "margin-bottom:0!important";
    } 


    if (properties.get(CONST.PROP_BOLD)) {
        title.marginb = "font-weight:bold!important";
    } 

    if (properties.get(CONST.PROP_UPPERCASE)) {
        title.uppercase = "text-transform: uppercase!important";
    }

    title.element = granite.resource.properties[CONST.PROP_TAG_TYPE]
            || currentStyle.get(CONST.PROP_DEFAULT_TYPE, "");

    title.fontcolor = granite.resource.properties[CONST.PROP_FONTCOLOR]
            || currentStyle.get(CONST.PROP_DEFAULT_FONTCOLOR, "");



    if (title.fontcolor == "fontcolor-white") {
        title.fontcolor = "color: #ffffff!important;";
    } 
       if (title.fontcolor == "fontcolor-black") {
        title.fontcolor = "color: #000000!important;";
    } 
       if (title.fontcolor == "fontcolor-green") {
        title.fontcolor = "color: #008651!important;";
    } 

    title.alignment = granite.resource.properties[CONST.PROP_ALIGN]
            || currentStyle.get(CONST.PROP_ALIGN, "");



    if (title.alignment == "left") {
        title.alignment = "text-align: left;";
    } 
       if (title.alignment == "right") {
        title.alignment = "text-align: right;";
    } 
       if (title.alignment == "center") {
        title.alignment = "text-align: center;";
    } 

    return title;

});